package colormixer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ColorMixerGUI {

	private JFrame frame;
	private JTextField txtColor1RED;
	private JTextField txtColor1GREEN;
	private JTextField txtColor1BLUE;
	private JTextField txtColor2RED;
	private JTextField txtColor2GREEN;
	private JTextField txtColor2BLUE;

	JPanel pnlColor1;
	JPanel pnlColor2;
	JPanel pnlColorResult;

	Color color1;
	Color color2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ColorMixerGUI window = new ColorMixerGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ColorMixerGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 4, 0, 0));

		JPanel color1Panel = new JPanel();
		frame.getContentPane().add(color1Panel);
		color1Panel.setLayout(new BorderLayout(0, 0));

		JPanel panel_2 = new JPanel();
		color1Panel.add(panel_2, BorderLayout.NORTH);

		txtColor1RED = new JTextField();
		panel_2.add(txtColor1RED);
		txtColor1RED.setColumns(3);
		txtColor1RED.addActionListener(new Color1ChangeActionListener());
		txtColor1RED.setText("0");

		txtColor1GREEN = new JTextField();
		panel_2.add(txtColor1GREEN);
		txtColor1GREEN.setColumns(3);
		txtColor1GREEN.addActionListener(new Color1ChangeActionListener());
		txtColor1GREEN.setText("0");

		txtColor1BLUE = new JTextField();
		panel_2.add(txtColor1BLUE);
		txtColor1BLUE.setColumns(3);
		txtColor1BLUE.addActionListener(new Color1ChangeActionListener());
		txtColor1BLUE.setText("0");

		JButton btnNewButton_1 = new JButton("Lighten");
		btnNewButton_1.addActionListener(new LightenColorActionListener());
		panel_2.add(btnNewButton_1);

		pnlColor1 = new JPanel();
		color1Panel.add(pnlColor1, BorderLayout.CENTER);

		JPanel color2Panel = new JPanel();
		frame.getContentPane().add(color2Panel);
		color2Panel.setLayout(new BorderLayout(0, 0));

		JPanel panel_3 = new JPanel();
		color2Panel.add(panel_3, BorderLayout.NORTH);

		txtColor2RED = new JTextField();
		panel_3.add(txtColor2RED);
		txtColor2RED.setColumns(3);
		txtColor2RED.addActionListener(new Color2ChangeActionListener());
		txtColor2RED.setText("0");

		txtColor2GREEN = new JTextField();
		panel_3.add(txtColor2GREEN);
		txtColor2GREEN.setColumns(3);
		txtColor2GREEN.addActionListener(new Color2ChangeActionListener());
		txtColor2GREEN.setText("0");

		txtColor2BLUE = new JTextField();
		panel_3.add(txtColor2BLUE);
		txtColor2BLUE.setColumns(3);
		txtColor2BLUE.addActionListener(new Color2ChangeActionListener());
		txtColor2BLUE.setText("0");

		pnlColor2 = new JPanel();
		color2Panel.add(pnlColor2, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(5, 1, 0, 0));

		JPanel panel_1 = new JPanel();
		panel.add(panel_1);

		JPanel panel_4 = new JPanel();
		panel.add(panel_4);

		JButton btnNewButton = new JButton("Mix colors");
		btnNewButton.addActionListener(new MixColorActionListener());
		panel.add(btnNewButton);

		JPanel colorResultPanel = new JPanel();
		frame.getContentPane().add(colorResultPanel);
		colorResultPanel.setLayout(new BorderLayout(0, 0));

		JPanel panel_5 = new JPanel();
		colorResultPanel.add(panel_5, BorderLayout.NORTH);

		JLabel lblNewLabel = new JLabel("New label");
		panel_5.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("New label");
		panel_5.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("New label");
		panel_5.add(lblNewLabel_2);

		pnlColorResult = new JPanel();
		colorResultPanel.add(pnlColorResult, BorderLayout.CENTER);
	}

	class Color1ChangeActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int red = Integer.parseInt(txtColor1RED.getText());
			int green = Integer.parseInt(txtColor1GREEN.getText());
			int blue = Integer.parseInt(txtColor1BLUE.getText());
			color1 = new Color(red, green, blue);
			pnlColor1.setBackground(color1);

		}

	}

	class Color2ChangeActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int red = Integer.parseInt(txtColor2RED.getText());
			int green = Integer.parseInt(txtColor2GREEN.getText());
			int blue = Integer.parseInt(txtColor2BLUE.getText());
			color2 = new Color(red, green, blue);
			pnlColor2.setBackground(color2);

		}

	}

	class MixColorActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// Color result = ColorMixer.mixTwoColor(color1, color2);
			List<Color> colors = new ArrayList<Color>();
			colors.add(color1);
			colors.add(color2);
			Color result = ColorMixer.mixColors(colors);
			pnlColorResult.setBackground(result);
		}

	}

	class LightenColorActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			color1 = ColorMixer.ligthenColorByPercent(color1, 0.1f);
			pnlColor1.setBackground(color1);
		}

	}

}
