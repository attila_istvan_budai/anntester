package colormixer;

import java.awt.Color;
import java.util.List;

public class ColorMixer {

	public static void main(String[] args) {
		Color col1 = Color.BLUE;
		Color col2 = Color.YELLOW;
		Color res = mixTwoColor(col1, col2);
	};

	public static Color mixTwoColor(Color c1, Color c2) {
		double[] color1RYB = rgbToRYB(c1.getRed(), c1.getGreen(), c1.getBlue());
		double[] color2RYB = rgbToRYB(c2.getRed(), c2.getGreen(), c2.getBlue());
		double[] resultRYB = new double[3];
		for (int i = 0; i < 3; i++) {
			resultRYB[i] = ((color1RYB[i] + color2RYB[i]) / 2);
		}
		double maximum = getMax(resultRYB);
		for (int i = 0; i < 3; i++) {
			resultRYB[i] = (resultRYB[i] / maximum) * (double) 255;
		}
		double[] resultRGB = rybToRGB(resultRYB[0], resultRYB[1], resultRYB[2]);
		return new Color((int) resultRGB[0], (int) resultRGB[1], (int) resultRGB[2]);
	}

	public static Color mixColors(List<Color> colors) {
		double[] resultRYB = new double[3];
		for (Color color : colors) {
			double[] colorRYB = rgbToRYB(color.getRed(), color.getGreen(), color.getBlue());
			for (int i = 0; i < 3; i++) {
				resultRYB[i] += colorRYB[i] / colors.size();
			}

		}
		double maximum = getMax(resultRYB);
		for (int i = 0; i < 3; i++) {
			resultRYB[i] = (resultRYB[i] / maximum) * (double) 255;
		}
		double[] resultRGB = rybToRGB(resultRYB[0], resultRYB[1], resultRYB[2]);
		return new Color((int) resultRGB[0], (int) resultRGB[1], (int) resultRGB[2]);

	}

	public static Color lightenColor(Color in, int amount) {
		int red = in.getRed() + amount;
		int green = in.getGreen() + amount;
		int blue = in.getBlue() + amount;
		return new Color(Math.min(red, 255), Math.min(green, 255), Math.min(blue, 255));
	}

	public static Color darkenColor(Color in, int amount) {
		int red = in.getRed() + amount;
		int green = in.getGreen() + amount;
		int blue = in.getBlue() + amount;
		return new Color(Math.max(red, 0), Math.max(green, 0), Math.max(blue, 0));
	}

	public static Color ligthenColorByPercent(Color in, float percent) {
		if (percent > 0) {
			return lightenColor(in, (int) (percent * 255));
		} else {
			return darkenColor(in, (int) (percent * 255));
		}
	}

	public static double getMax(double[] array) {
		double max = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}

		}
		return max;
	}

	public static double[] rgbToRYB(double R, double G, double B) {
		// remove the whiteness from the color
		double w = Math.min(Math.min(R, G), B);
		double r = R - w;
		double g = G - w;
		double b = B - w;

		double mg = Math.max(Math.max(r, g), b);

		// get the yellow out of the red and green
		double y = Math.min(r, g);
		r -= y;
		g -= y;

		// if this unfortunate conversion combines blue and green, then cut each
		// in half to preserve the value's maximum range
		if (b != 0 && g != 0) {
			b /= 2.0;
			g /= 2.0;
		}

		// redistribute the remaining green
		y += g;
		b += g;

		// normalize to values
		double my = Math.max(r, Math.max(y, b));
		if (my != 0) {
			double n = mg / my;
			r *= n;
			y *= n;
			b *= n;
		}

		// add back in white
		r += w;
		y += w;
		b += w;

		double[] RYB = new double[3];
		RYB[0] = r;
		RYB[1] = y;
		RYB[2] = b;

		return RYB;
	}

	// credit to http://www.insanit.net/tag/rgb-to-ryb/
	public static double[] rybToRGB(double R, double Y, double B) {
		// remove the whiteness from the color
		double w = Math.min(Math.min(R, Y), B);
		double r = R - w;
		double y = Y - w;
		double b = B - w;

		double my = Math.max(Math.max(r, y), b);

		// get the green out of the yellow and blue
		double g = Math.min(y, b);
		y -= g;
		b -= g;

		if (b != 0 && g != 0) {
			b *= 2.0;
			g *= 2.0;
		}

		// redistribute the remaining yellow
		r += y;
		g += y;

		// normalize to values
		double mg = Math.max(Math.max(r, g), b);
		if (mg != 0) {
			double n = my / mg;
			r *= n;
			g *= n;
			b *= n;
		}

		// add the white back in
		r += w;
		g += w;
		b += w;

		double[] RGB = new double[3];
		RGB[0] = r;
		RGB[1] = g;
		RGB[2] = b;

		return RGB;
	}

}