package utils;

import javax.swing.JProgressBar;

import view.WriteNumbersModel;

import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.Subject;
import com.bme.datamining.logging.observer.simpleobserver.RBMResultObject;

public class RBMViewObserver implements Observer {

	private Subject topic;
	JProgressBar progressbar;
	int maxIterations;
	WriteNumbersModel model;

	public RBMViewObserver(WriteNumbersModel model) {
		this.progressbar = model.getView().getParameterPanel().getProgressBar();
		this.maxIterations = model.getIterationNumber();
		this.model = model;
	}

	@Override
	public void update() {
		RBMResultObject msg = (RBMResultObject) topic.getUpdate(this);
		if (msg == null) {
			System.out.println(" No new message");
		} else {
			printResult(msg);
			model.setWeight(msg.getModel());
			// model.showSeperatedModel(msg.getModel());
		}
	}

	public void printResult(RBMResultObject msg) {
		progressbar.setValue(msg.getIterationNumber() * 100 / maxIterations);
		model.addElementToResultGraph(msg.getError(), msg.getIterationNumber());
	}

	public void showViewModel() {
	}

	@Override
	public void setSubject(Subject sub) {
		this.topic = sub;
	}
}
