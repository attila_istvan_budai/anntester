package mnistreader;

import java.io.File;
import java.util.List;

public class Tester {

	public static void main(String[] args) {
		File labelFile = new File("C:\\mnist\\train-labels-idx1-ubyte.gz");
		File imageFile = new File("C:\\mnist\\train-images-idx3-ubyte.gz");
		MinstDatasetReader reader = new MinstDatasetReader(labelFile, imageFile);

		System.out.println("test size: " + reader.testSet.size());
		for (List<MinstItem> item : reader.testSet.values()) {
			System.out.println("item size " + item.size());
		}
		System.out.println("training size: " + reader.trainingSet.values().size());
		List<MinstItem> items = reader.getAllTrainingItem(3);

		for (MinstItem item : items) {
			float[] data = item.data;
			for (int i = 0; i < 28 * 28; i++) {
				// System.out.print(data[i] + " ");
				System.out.format("%5f", data[i]);
				if (i % 28 == 0)
					System.out.println();
			}
		}

	}

}
