package dynamicpanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class DynamicCell extends JPanel {
	int row;
	int col;
	DynamicPanel parentPane;
	private Color backgroundColor;
	boolean isEditable;

	int preferredWidth;
	int preferredHeight;

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public DynamicCell(DynamicPanel t, int c, int r, boolean isEditable) {
		this.row = r;
		this.col = c;
		this.parentPane = t;
		this.isEditable = isEditable;
		setPreferredSizes();
		if (isEditable) {
			addMouseListener(new MouseAdapter() {
				@Override
				public void mouseEntered(MouseEvent e) {
					if (SwingUtilities.isLeftMouseButton(e)) {
						setBackground(Color.BLACK);
						parentPane.getModel().updateModel(1f, col, row);
					} else if (SwingUtilities.isRightMouseButton(e)) {
						setBackground(Color.WHITE);
						parentPane.getModel().updateModel(0f, col, row);
					}

				}

				@Override
				public void mousePressed(MouseEvent e) {
					if (SwingUtilities.isLeftMouseButton(e)) {
						setBackground(Color.BLACK);
						parentPane.getModel().updateModel(1f, col, row);
					} else if (SwingUtilities.isRightMouseButton(e)) {
						setBackground(Color.WHITE);
						parentPane.getModel().updateModel(0f, col, row);
					}

				}
			});
		}
	}

	public void updateBasedOnModel() {
		Color color = parentPane.getModel().getCellColors().get(row * parentPane.getColumnNumber() + col);
		setBackground(color);

	}

	public void setPreferredSizes() {
		preferredWidth = (int) ((double) parentPane.getWidth() / (double) parentPane.getColumnNumber());
		preferredHeight = (int) ((double) parentPane.getHeight() / (double) parentPane.getRowNumber());
		this.setSize(preferredWidth, preferredHeight);
	}

	@Override
	public Dimension getPreferredSize() {
		if (preferredWidth != 0) {
			return new Dimension(preferredWidth, preferredHeight);
		} else
			return new Dimension(50, 50);
	}
}
