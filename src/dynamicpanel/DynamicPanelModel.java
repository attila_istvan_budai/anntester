package dynamicpanel;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import colormixer.ColorMixer;

public class DynamicPanelModel {
	List<Color> inputColors;
	List<Color> cellColors;
	int hiddenNumber;
	float[] model;

	private int columnNumber;
	private int rowNumber;

	private DynamicPanelModel(int hiddenNumber, float[] model, int columnNumber, int rowNumber, List<Color> inputColors) {
		this.hiddenNumber = hiddenNumber;

		this.columnNumber = columnNumber;
		this.rowNumber = rowNumber;
		this.inputColors = inputColors;
		cellColors = new ArrayList<Color>();
		for (int i = 0; i < columnNumber * rowNumber; i++) {
			cellColors.add(Color.WHITE);
		}
		if (model != null) {
			this.model = model;
			calculateCellColors();
		}

	}

	public static class DynamicPanelModelBuilder {
		List<Color> inputColors;
		List<Color> cellColors;
		int hiddenNumber = 1;
		float[] model;
		private int columnNumber;
		private int rowNumber;

		public DynamicPanelModelBuilder setCellNumber(int colNumber, int rowNumber) {
			this.columnNumber = colNumber;
			this.rowNumber = rowNumber;
			return this;
		}

		public DynamicPanelModelBuilder setInputColors(List<Color> inputColors) {
			this.inputColors = inputColors;
			return this;
		}

		public DynamicPanelModelBuilder setOneInputColor(Color color) {
			this.inputColors = new ArrayList<Color>();
			inputColors.add(color);
			return this;
		}

		public DynamicPanelModelBuilder setModel(float[] model) {
			this.model = model;
			return this;
		}

		public DynamicPanelModelBuilder setHiddenNumber(int hiddenNum) {
			this.hiddenNumber = hiddenNum;
			return this;
		}

		public DynamicPanelModel createDynamicPanel() {
			if (inputColors == null) {
				inputColors = new ArrayList<Color>();
				inputColors.add(Color.RED);
				inputColors.add(Color.BLUE);
				inputColors.add(Color.YELLOW);
				inputColors.add(Color.GREEN);
				inputColors.add(Color.ORANGE);

				inputColors.add(Color.GREEN);
				inputColors.add(Color.MAGENTA);
				inputColors.add(Color.PINK);
				inputColors.add(Color.CYAN);
				inputColors.add(Color.LIGHT_GRAY);
			}
			if (model == null) {
				model = new float[columnNumber * rowNumber];
			}
			return new DynamicPanelModel(hiddenNumber, model, columnNumber, rowNumber, inputColors);
		}

	}

	public List<Color> getInputColors() {
		return inputColors;
	}

	public void setInputColors(List<Color> inputColors) {
		this.inputColors = inputColors;
	}

	public List<Color> getCellColors() {
		return cellColors;
	}

	public void setCellColors(List<Color> cellColors) {
		this.cellColors = cellColors;
	}

	public int getHiddenNumber() {
		return hiddenNumber;
	}

	public void setHiddenNumber(int hiddenNumber) {
		this.hiddenNumber = hiddenNumber;
	}

	public float[] getModel() {
		return model;
	}

	public void setModel(float[] model, int columnNumber, int rowNumber) {
		this.model = model;
		this.columnNumber = columnNumber;
		this.rowNumber = rowNumber;
		calculateCellColors();
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public void setColumnNumber(int columnNumber) {
		this.columnNumber = columnNumber;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public void updateModel(float value, int col, int row) {
		int index = row * columnNumber + col;
		this.model[index] = value;
		Color color = ColorMixer.ligthenColorByPercent(inputColors.get(0), (1 - value));
		cellColors.set(index, color);
	}

	public void calculateCellColors() {
		cellColors = new ArrayList<Color>();
		for (int i = 0; i < model.length; i += hiddenNumber) {
			List<Color> colors = new ArrayList<Color>();
			for (int j = 0; j < hiddenNumber; j++) {
				Color color = ColorMixer.ligthenColorByPercent(inputColors.get(j), (1 - (model[i + j])));
				colors.add(color);
			}
			Color mixed = ColorMixer.mixColors(colors);
			cellColors.add(mixed);
		}
	}

}
