package dynamicpanel;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;

public class DynamicPanel extends JPanel {
	private int columnNumber;
	private int rowNumber;
	private int hiddenNumber;
	private DynamicPanelModel model;
	private List<DynamicCell> cells;
	boolean isEditable;
	Color color;

	private int width;
	private int height;

	private DynamicPanel(int columns, int rows, int width, int height, boolean isEditable, Color color) {
		this.columnNumber = columns;
		this.rowNumber = rows;
		this.setSize(width, height);
		DynamicPanelModel.DynamicPanelModelBuilder builder = new DynamicPanelModel.DynamicPanelModelBuilder();
		builder.setCellNumber(columns, rows);
		if (color != null) {
			builder.setOneInputColor(color);
		}
		setModel(builder.createDynamicPanel());
		this.isEditable = isEditable;
		initParams();
		initializeCells();

	}

	private void initParams() {
		cells = new ArrayList<DynamicCell>();

		setLayout(new GridBagLayout());
	}

	public static class DynamicPanelBuilder {
		private int columnNumber = 10;
		private int rowNumber = 10;
		private int width = 250;
		private int height = 250;
		private int hiddenNumber = 1;
		private DynamicPanelModel model;
		private List<DynamicCell> cells;
		boolean isEditable;
		Color color;

		public DynamicPanelBuilder setCellNumber(int colNumber, int rowNumber) {
			this.columnNumber = colNumber;
			this.rowNumber = rowNumber;
			return this;
		}

		public DynamicPanelBuilder setSizes(int width, int height) {
			this.width = width;
			this.height = height;
			return this;
		}

		public DynamicPanelBuilder editable(boolean isEditable) {
			this.isEditable = isEditable;
			return this;
		}

		public DynamicPanelBuilder setHiddenNumber(int hiddenNum) {
			this.hiddenNumber = hiddenNum;
			return this;
		}

		public DynamicPanelBuilder setColor(Color color) {
			this.color = color;
			return this;
		}

		public DynamicPanel createDynamicPanel() {
			return new DynamicPanel(columnNumber, rowNumber, width, height, isEditable, color);
		}

	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public void setColumnNumber(int columnNumber) {
		this.columnNumber = columnNumber;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public void setEditable(boolean editable) {
		this.isEditable = editable;
		for (DynamicCell cell : cells) {
			cell.isEditable = editable;
		}
	}

	private void initializeCells() {
		GridBagConstraints gbc = new GridBagConstraints();
		for (int row = 0; row < rowNumber; row++) {
			for (int col = 0; col < columnNumber; col++) {
				gbc.gridx = col;
				gbc.gridy = row;
				DynamicCell cellPane = new DynamicCell(this, col, row, isEditable);
				cells.add(cellPane);
				Border border = null;
				if (row < rowNumber - 1) {
					if (col < columnNumber - 1) {
						border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
					} else {
						border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
					}
				} else {
					if (col < columnNumber - 1) {
						border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
					} else {
						border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
					}
				}
				cellPane.setBorder(border);
				add(cellPane, gbc);
			}

		}
		revalidate();
	}

	public void refresh() {
		for (DynamicCell cell : cells) {
			cell.updateBasedOnModel();
		}
	}

	public void reset() {
		float[] data = new float[columnNumber * rowNumber];
		// DenseMatrix matrix = new DenseMatrix(data, columnNumber, rowNumber);
		updateCells(data, 1);
	}

	public DynamicPanelModel getModel() {
		return model;
	}

	public void setModel(DynamicPanelModel model) {
		this.model = model;
	}

	public void updateCells(float[] data, int hiddenNumber) {
		this.model.setModel(data, columnNumber, rowNumber);
		this.model.setHiddenNumber(hiddenNumber);
		this.model.calculateCellColors();
		for (DynamicCell cell : cells) {
			cell.updateBasedOnModel();
		}
	}

	// @Override
	// public Dimension getPreferredSize() {
	// return new Dimension(width, height);
	// }
}
