package view;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import view.actionlisteners.CheckGuessedNumMenuActionListener;
import view.actionlisteners.CheckMenuActionListener;
import view.components.DetailedResultView;
import view.components.MyMenubar;
import view.components.ParameterPanel;
import view.components.ResultChart;
import view.components.TrainingPanelView;
import view.util.Utils;
import dynamicpanel.DynamicPanel;

public class WriteNumbers extends JFrame {
	MyMenubar menuBar;
	private ParameterPanel parameterPanel;
	private TrainingPanelView trainingPanelView;
	private ResultChart resultChart;
	private ResultChart resultChartZoomed;

	private JPanel mainPanel;
	private JPanel result;
	private DetailedResultView detailedResultView;

	JScrollPane detailedResultScrollPane;

	List<DynamicPanel> detailedResultPanels;
	DynamicPanel inputPane;
	DynamicPanel resultPane;
	DynamicPanel modelResultPane;

	WriteNumbersModel model;

	JLabel lblGuessTheNumber;

	int widthOfPanels = 28;
	int heightOfPanels = 28;

	private final int panelNumber = 5;
	private int visibleFrom = 0;
	private int visibleTo = getPanelNumber();

	public WriteNumbers(String name) {
		super(name);
		model = new WriteNumbersModel(this);
	}

	public DynamicPanel getInputPane() {
		return inputPane;
	}

	public void setInputPane(DynamicPanel inputPane) {
		this.inputPane = inputPane;
	}

	public DynamicPanel getResultPane() {
		return resultPane;
	}

	public void setResultPane(DynamicPanel resultPane) {
		this.resultPane = resultPane;
	}

	public DynamicPanel getModelResultPane() {
		return modelResultPane;
	}

	public void setModelResultPane(DynamicPanel modelResultPane) {
		this.modelResultPane = modelResultPane;
	}

	public int getWidthOfPanels() {
		return widthOfPanels;
	}

	public void setWidthOfPanels(int widthOfPanels) {
		this.widthOfPanels = widthOfPanels;
	}

	public int getHeightOfPanels() {
		return heightOfPanels;
	}

	public void setHeightOfPanels(int heightOfPanels) {
		this.heightOfPanels = heightOfPanels;
	}

	public void initialize(final Container pane) {

		JTabbedPane tabbedPane = new JTabbedPane();

		writeNumberGUI();

		tabbedPane.add(getMainPanel(), "Test with numbers");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

		tabbedPane.add(createGraphPanel(), "Result graph");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

		pane.add(tabbedPane);

		Utils.enableComponents(getResult(), true);

	}

	public JPanel createGraphPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		resultChart = new ResultChart("Result RMSE", 1000);
		resultChartZoomed = new ResultChart("Result RMSE Zoomed", 10);

		panel.add(resultChart);
		panel.add(resultChartZoomed);

		return panel;

	}

	public void writeNumberGUI() {
		// menus
		menuBar = new MyMenubar(model, this);
		this.setJMenuBar(menuBar);

		// main panel
		setMainPanel(new JPanel());
		getMainPanel().setLayout(new BoxLayout(getMainPanel(), BoxLayout.Y_AXIS));

		// learning parameters
		setParameterPanel(new ParameterPanel(model, this));

		getMainPanel().add(getParameterPanel());

		// draw panels
		setTrainingPanelView(new TrainingPanelView(model, this));
		getMainPanel().add(getTrainingPanelView());

		JButton btnCheckResult = new JButton();
		btnCheckResult.setText("Check result" + "\u25BA");
		btnCheckResult.addActionListener(new CheckMenuActionListener(model));

		JButton btnGuessTheNumberResult = new JButton();
		btnGuessTheNumberResult.setText("Guess the numbert" + "\u25BA");
		btnGuessTheNumberResult.addActionListener(new CheckGuessedNumMenuActionListener(model));

		lblGuessTheNumber = new JLabel("The guessed number");

		setResult(new JPanel());
		getResult().setLayout(new BoxLayout(getResult(), BoxLayout.X_AXIS));
		JPanel panelinput = new JPanel();
		panelinput.setLayout(new BoxLayout(panelinput, BoxLayout.Y_AXIS));
		inputPane = new DynamicPanel.DynamicPanelBuilder().setSizes(300, 300).setCellNumber(widthOfPanels, heightOfPanels).editable(true).createDynamicPanel();
		JLabel inputLabel = new JLabel("Input params:");
		panelinput.add(inputLabel);
		panelinput.add(inputPane);

		JPanel panelresult = new JPanel();
		panelresult.setLayout(new BoxLayout(panelresult, BoxLayout.Y_AXIS));
		resultPane = new DynamicPanel.DynamicPanelBuilder().setSizes(300, 300).setCellNumber(widthOfPanels, heightOfPanels).editable(false).createDynamicPanel();
		JLabel resultLabel = new JLabel("The result of the input:");
		panelresult.add(resultLabel);
		panelresult.add(resultPane);

		JPanel panelmodelresult = new JPanel();
		panelmodelresult.setLayout(new BoxLayout(panelmodelresult, BoxLayout.Y_AXIS));
		modelResultPane = new DynamicPanel.DynamicPanelBuilder().setSizes(300, 300).setCellNumber(widthOfPanels, heightOfPanels).editable(false).createDynamicPanel();
		JLabel modelLabel = new JLabel("The model:");
		panelmodelresult.add(modelLabel);
		panelmodelresult.add(modelResultPane);

		getResult().add(panelinput);
		getResult().add(btnCheckResult);
		getResult().add(panelresult);
		getResult().add(btnGuessTheNumberResult);
		getResult().add(lblGuessTheNumber);
		getResult().add(panelmodelresult);
		getMainPanel().add(getResult());

		setDetailedResultView(new DetailedResultView(this));
		detailedResultScrollPane = new JScrollPane(getDetailedResultView());
		getMainPanel().add(detailedResultScrollPane);
	}

	/**
	 * Create the GUI and show it. For thread safety, this method is invoked
	 * from the event dispatch thread.
	 */
	private static void createAndShowGUI() {
		// Create and set up the window.
		WriteNumbers frame = new WriteNumbers("WriteNumbers");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Set up the content pane.
		frame.initialize(frame.getContentPane());

		frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		/* Use an appropriate Look and Feel */
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			// UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		/* Turn off metal's use of bold fonts */
		UIManager.put("swing.boldMetal", Boolean.FALSE);

		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public void setTrainingPanelsEditable(boolean editable) {
		List<DynamicPanel> trainingPanels = getTrainingPanelView().getTrainingPanels();
		for (DynamicPanel p : trainingPanels) {
			p.setEditable(editable);
		}
	}

	public void refreshAll() {
		List<DynamicPanel> trainingPanels = getTrainingPanelView().getTrainingPanels();
		for (DynamicPanel p : trainingPanels) {
			p.refresh();
		}
	}

	public JPanel getResult() {
		return result;
	}

	public void setResult(JPanel result) {
		this.result = result;
	}

	public int getVisibleFrom() {
		return visibleFrom;
	}

	public void setVisibleFrom(int visibleFrom) {
		this.visibleFrom = visibleFrom;
	}

	public int getVisibleTo() {
		return visibleTo;
	}

	public void setVisibleTo(int visibleTo) {
		this.visibleTo = visibleTo;
	}

	public int getPanelNumber() {
		return panelNumber;
	}

	public ParameterPanel getParameterPanel() {
		return parameterPanel;
	}

	public void setParameterPanel(ParameterPanel parameterPanel) {
		this.parameterPanel = parameterPanel;
	}

	public TrainingPanelView getTrainingPanelView() {
		return trainingPanelView;
	}

	public void setTrainingPanelView(TrainingPanelView trainingPanelView) {
		this.trainingPanelView = trainingPanelView;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public void setMainPanel(JPanel mainPanel) {
		this.mainPanel = mainPanel;
	}

	public DetailedResultView getDetailedResultView() {
		return detailedResultView;
	}

	public void setDetailedResultView(DetailedResultView detailedResultView) {
		this.detailedResultView = detailedResultView;
	}

	public ResultChart getResultChart() {
		return resultChart;
	}

	public void setResultChart(ResultChart resultChart) {
		this.resultChart = resultChart;
	}

	public ResultChart getResultChartZoomed() {
		return resultChartZoomed;
	}

	public void setResultChartZoomed(ResultChart resultChartZoomed) {
		this.resultChartZoomed = resultChartZoomed;
	}

}
