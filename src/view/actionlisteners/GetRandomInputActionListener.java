package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.WriteNumbersModel;

public class GetRandomInputActionListener implements ActionListener {
	WriteNumbersModel model;

	public GetRandomInputActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	public void actionPerformed(ActionEvent e) {
		model.setRandomInput();
	}
}