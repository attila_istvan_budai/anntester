package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.WriteNumbers;
import view.WriteNumbersModel;

public class OpenMniskMenuActionListener implements ActionListener {

	WriteNumbersModel model;
	WriteNumbers view;

	public OpenMniskMenuActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	public void actionPerformed(ActionEvent e) {
		view.setTrainingPanelsEditable(false);
		model.readFromMnist();
		view.refreshAll();

	}
}
