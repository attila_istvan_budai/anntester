package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.WriteNumbers;
import view.WriteNumbersModel;

public class NextPanelsActionListener implements ActionListener {

	WriteNumbersModel model;
	WriteNumbers view;

	public NextPanelsActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	public void actionPerformed(ActionEvent e) {
		view.setVisibleFrom(view.getVisibleFrom() + view.getPanelNumber());
		view.setVisibleTo(view.getVisibleTo() + view.getPanelNumber());
		model.setVisiblePanels(view.getVisibleFrom(), view.getVisibleTo());
		view.refreshAll();
	}
}