package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;

import view.WriteNumbersModel;

public class SaveMenuActionListener implements ActionListener {

	WriteNumbersModel model;

	public SaveMenuActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	public void actionPerformed(ActionEvent e) {
		JFileChooser saveFile = new JFileChooser();
		int choose = saveFile.showSaveDialog(null);
		if (choose == 0) {
			model.writeToFile(saveFile.getSelectedFile());
		}
	}
}
