package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.WriteNumbersModel;

public class CheckMenuActionListener implements ActionListener {

	WriteNumbersModel model;

	public CheckMenuActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	public void actionPerformed(ActionEvent e) {
		model.check();
	}
}
