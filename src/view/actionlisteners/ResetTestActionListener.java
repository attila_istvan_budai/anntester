package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.WriteNumbers;

public class ResetTestActionListener implements ActionListener {

	WriteNumbers view;

	public ResetTestActionListener(WriteNumbers view) {
		this.view = view;
	}

	public void actionPerformed(ActionEvent e) {
		view.getInputPane().reset();
	}
}