package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.WriteNumbersModel;

public class CheckGuessedNumMenuActionListener implements ActionListener {

	WriteNumbersModel model;

	public CheckGuessedNumMenuActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	public void actionPerformed(ActionEvent e) {
		model.guessNumber();
	}
}
