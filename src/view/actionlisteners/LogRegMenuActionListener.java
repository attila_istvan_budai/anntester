package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.SwingWorker;

import view.WriteNumbers;
import view.WriteNumbersModel;

public class LogRegMenuActionListener implements ActionListener {

	WriteNumbersModel model;
	WriteNumbers view;

	public LogRegMenuActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	public void actionPerformed(ActionEvent e) {
		try {
			LearnWorker worker = new LearnWorker();
			worker.execute();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

	class LearnWorker extends SwingWorker<Void, Void> {

		@Override
		public Void doInBackground() throws IOException {
			model.learnFromModel();
			return null;
		}

		@Override
		public void done() {
		}
	}

}
