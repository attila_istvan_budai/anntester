package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.WriteNumbers;
import dynamicpanel.DynamicPanel;

public class ResetActionListener implements ActionListener {

	WriteNumbers view;

	public ResetActionListener(WriteNumbers view) {
		this.view = view;
	}

	public void actionPerformed(ActionEvent e) {
		for (DynamicPanel p : view.getTrainingPanelView().getTrainingPanels()) {
			p.reset();
		}
	}
}
