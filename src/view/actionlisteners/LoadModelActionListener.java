package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import view.WriteNumbersModel;

public class LoadModelActionListener implements ActionListener {

	WriteNumbersModel model;

	public LoadModelActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	public void actionPerformed(ActionEvent e) {
		model.loadModelFromFile(new File("G:\\modelResult.dat"));
		model.showModel();
	}
}
