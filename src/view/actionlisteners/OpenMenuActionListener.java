package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;

import view.WriteNumbersModel;

public class OpenMenuActionListener implements ActionListener {
	WriteNumbersModel model;

	public OpenMenuActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	public void actionPerformed(ActionEvent e) {
		JFileChooser openFile = new JFileChooser();
		int choose = openFile.showOpenDialog(null);
		if (choose == 0) {
			model.readFromFile(openFile.getSelectedFile());
		}
	}
}