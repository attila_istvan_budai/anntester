package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.SwingWorker;

import view.WriteNumbers;
import view.WriteNumbersModel;
import view.util.Utils;

public class LearnMenuActionListener implements ActionListener {

	WriteNumbersModel model;
	WriteNumbers view;

	public LearnMenuActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	public void actionPerformed(ActionEvent e) {
		try {
			LearnWorker worker = new LearnWorker();
			worker.execute();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

	class LearnWorker extends SwingWorker<Void, Void> {

		@Override
		public Void doInBackground() throws IOException {
			model.learn();
			return null;
		}

		@Override
		public void done() {
			Utils.enableComponents(view.getResult(), true);
			// view.getDetailedResultView().initDetailedResultPanels(model.getHiddenNumber());
			try {
				model.saveDnnResult();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			model.showModel();
		}
	}

}
