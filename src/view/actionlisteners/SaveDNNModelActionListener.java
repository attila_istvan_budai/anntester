package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import view.WriteNumbersModel;

public class SaveDNNModelActionListener implements ActionListener {
	WriteNumbersModel model;

	public SaveDNNModelActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	public void actionPerformed(ActionEvent e) {
		try {
			model.saveDnnResult();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
