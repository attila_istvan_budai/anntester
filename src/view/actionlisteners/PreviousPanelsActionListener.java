package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.WriteNumbers;
import view.WriteNumbersModel;

public class PreviousPanelsActionListener implements ActionListener {

	WriteNumbersModel model;
	WriteNumbers view;

	public PreviousPanelsActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	public void actionPerformed(ActionEvent e) {
		if (view.getVisibleFrom() > view.getPanelNumber()) {
			view.setVisibleFrom(view.getVisibleFrom() - view.getPanelNumber());
			view.setVisibleTo(view.getVisibleTo() - view.getPanelNumber());

		} else {
			view.setVisibleFrom(0);
			view.setVisibleTo(view.getPanelNumber());
		}
		model.setVisiblePanels(view.getVisibleFrom(), view.getVisibleTo());
		view.refreshAll();
	}
}
