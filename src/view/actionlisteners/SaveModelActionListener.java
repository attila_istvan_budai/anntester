package view.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import view.WriteNumbersModel;

public class SaveModelActionListener implements ActionListener {

	WriteNumbersModel model;

	public SaveModelActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	public void actionPerformed(ActionEvent e) {
		model.saveModelToFile(new File("G:\\modelResult.dat"));
	}
}