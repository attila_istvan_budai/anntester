package view.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.util.ArrayList;

public class Utils {

	public static void enableComponents(Container container, boolean enable) {
		Component[] components = container.getComponents();
		for (Component component : components) {
			component.setEnabled(enable);
			if (component instanceof Container) {
				enableComponents((Container) component, enable);
			}
		}
	}

	public static ArrayList<Color> getColors() {
		ArrayList<Color> inputColors = new ArrayList<Color>();
		inputColors.add(Color.RED);
		inputColors.add(Color.BLUE);
		inputColors.add(Color.YELLOW);
		inputColors.add(Color.GREEN);
		inputColors.add(Color.ORANGE);

		inputColors.add(Color.GREEN);
		inputColors.add(Color.MAGENTA);
		inputColors.add(Color.PINK);
		inputColors.add(Color.CYAN);
		inputColors.add(Color.LIGHT_GRAY);
		return inputColors;
	}

}
