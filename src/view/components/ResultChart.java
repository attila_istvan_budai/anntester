package view.components;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class ResultChart extends JPanel {
	/** The time series data. */
	private XYSeries series;
	private int size = 0;
	private String title;
	private int movingWindowSize;

	/**
	 * Constructs a new demonstration application.
	 *
	 * @param title
	 *            the frame title.
	 */
	public ResultChart(final String title, int movingWindowSize) {
		this.title = title;
		this.movingWindowSize = movingWindowSize;
		this.series = new XYSeries("Random Data");
		final XYSeriesCollection dataset = new XYSeriesCollection(this.series);
		final JFreeChart chart = createChart(dataset);

		final ChartPanel chartPanel = new ChartPanel(chart);
		this.add(chartPanel);
		// chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));

	}

	/**
	 * Creates a sample chart.
	 * 
	 * @param dataset
	 *            the dataset.
	 * 
	 * @return A sample chart.
	 */
	private JFreeChart createChart(final XYDataset dataset) {
		// final JFreeChart result =
		// ChartFactory.createTimeSeriesChart("Dynamic Data Demo", "Time",
		// "Value", dataset, true, true, false);
		final JFreeChart result = ChartFactory.createXYLineChart(title, // chart
																		// title
				"Iteration", // x axis label
				"RMSE", // y axis label
				dataset, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips
				false // urls
				);
		final XYPlot plot = result.getXYPlot();
		return result;
	}

	// ****************************************************************************
	// * JFREECHART DEVELOPER GUIDE *
	// * The JFreeChart Developer Guide, written by David Gilbert, is available
	// *
	// * to purchase from Object Refinery Limited: *
	// * *
	// * http://www.object-refinery.com/jfreechart/guide.html *
	// * *
	// * Sales are used to provide funding for the JFreeChart project - please *
	// * support us so that we can continue developing free software. *
	// ****************************************************************************

	public void addData(double d, int iteration) {
		size++;
		if (size > movingWindowSize) {
			series.remove(0);
		}
		series.add(iteration, d);

	}

	public class DataPoint {
		public double rmse;
		public int iteration;

		public DataPoint(double rmse, int iteration) {
			super();
			this.rmse = rmse;
			this.iteration = iteration;
		}

	}

}