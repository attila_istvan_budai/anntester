package view.components;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import view.WriteNumbers;
import view.WriteNumbersModel;
import view.actionlisteners.NextPanelsActionListener;
import view.actionlisteners.PreviousPanelsActionListener;
import dynamicpanel.DynamicPanel;

public class TrainingPanelView extends JPanel {

	List<DynamicPanel> trainingPanels;

	public TrainingPanelView(WriteNumbersModel model, WriteNumbers view) {
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		JPanel controls = new JPanel();
		JScrollPane controlsPanel = new JScrollPane(controls);

		JButton btnPrevious = new JButton();
		btnPrevious.setText("Prev");
		btnPrevious.addActionListener(new PreviousPanelsActionListener(model, view));

		JButton btnNext = new JButton();
		btnNext.setText("Next");
		btnNext.addActionListener(new NextPanelsActionListener(model, view));

		controls.setLayout(new GridLayout(1, view.getPanelNumber()));
		trainingPanels = new ArrayList<DynamicPanel>();

		for (int i = 0; i < view.getPanelNumber(); i++) {
			JPanel trainPanel = new JPanel();
			trainPanel.setSize(300, 300);
			DynamicPanel p = new DynamicPanel.DynamicPanelBuilder().setSizes(300, 300).setCellNumber(view.getWidthOfPanels(), view.getHeightOfPanels()).editable(true)
					.createDynamicPanel();
			trainingPanels.add(p);
			trainPanel.add(p);
			controls.add(trainPanel);
		}

		btnPrevious.setSize(20, controls.HEIGHT);
		this.add(btnPrevious);
		this.add(controlsPanel);
		btnNext.setSize(20, controls.HEIGHT);
		this.add(btnNext);
	}

	public List<DynamicPanel> getTrainingPanels() {
		return trainingPanels;
	}

	public void setTrainingPanels(List<DynamicPanel> trainingPanels) {
		this.trainingPanels = trainingPanels;
	}

}
