package view.components;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import view.WriteNumbers;
import view.util.Utils;
import dynamicpanel.DynamicPanel;

public class DetailedResultView extends JPanel {

	List<DynamicPanel> detailedResultPanels;

	WriteNumbers view;

	public DetailedResultView(WriteNumbers view) {
		this.view = view;
	}

	public List<DynamicPanel> getDetailedResultPanels() {
		return detailedResultPanels;
	}

	public void setDetailedResultPanels(List<DynamicPanel> detailedResultPanels) {
		this.detailedResultPanels = detailedResultPanels;
	}

	public void initDetailedResultPanels(int numberOfChildren) {
		this.removeAll();
		detailedResultPanels = new ArrayList<DynamicPanel>();
		for (int i = 0; i < numberOfChildren; i++) {
			// JPanel p1 = new JPanel();
			Color color = Utils.getColors().get(i);
			DynamicPanel p1 = new DynamicPanel.DynamicPanelBuilder().setSizes(300, 300).setCellNumber(view.getWidthOfPanels(), view.getHeightOfPanels()).editable(false)
					.setColor(color).createDynamicPanel();
			detailedResultPanels.add(p1);
			this.add(p1);
		}
		this.revalidate();
		view.getMainPanel().revalidate();

		// mainPanel.add(detailedResultScrollPane);
	}

}
