package view.components;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import view.WriteNumbers;
import view.WriteNumbersModel;
import view.actionlisteners.CheckMenuActionListener;
import view.actionlisteners.LearnMenuActionListener;
import view.actionlisteners.LoadModelActionListener;
import view.actionlisteners.LogRegMenuActionListener;
import view.actionlisteners.OpenDNNModelActionListener;
import view.actionlisteners.OpenMenuActionListener;
import view.actionlisteners.OpenMniskMenuActionListener;
import view.actionlisteners.SaveDNNModelActionListener;
import view.actionlisteners.SaveMenuActionListener;
import view.actionlisteners.SaveModelActionListener;

public class MyMenubar extends JMenuBar {

	JMenu menu;
	JMenu mnistMenu;
	JMenu dnnMenu;

	JMenuItem learn;
	JMenuItem check;
	JMenuItem open;
	JMenuItem save;

	JMenuItem openMnist;
	JMenuItem saveModel;
	JMenuItem loadModel;
	JMenuItem learnFromModel;

	JMenuItem openDNNModel;
	JMenuItem saveDNNModel;

	public MyMenubar(WriteNumbersModel model, WriteNumbers view) {
		menu = new JMenu("File..");
		this.add(menu);
		mnistMenu = new JMenu("Mnist..");
		this.add(mnistMenu);
		dnnMenu = new JMenu("DNN..");
		this.add(dnnMenu);

		learn = new JMenuItem("Learn");
		learn.addActionListener(new LearnMenuActionListener(model, view));
		menu.add(learn);

		check = new JMenuItem("Check");
		check.addActionListener(new CheckMenuActionListener(model));
		menu.add(check);

		open = new JMenuItem("Open");
		open.addActionListener(new OpenMenuActionListener(model));
		menu.add(open);

		save = new JMenuItem("Save");
		save.addActionListener(new SaveMenuActionListener(model));
		menu.add(save);

		openMnist = new JMenuItem("Open Mnist");
		openMnist.addActionListener(new OpenMniskMenuActionListener(model, view));
		mnistMenu.add(openMnist);

		saveModel = new JMenuItem("Save Model");
		saveModel.addActionListener(new SaveModelActionListener(model));
		mnistMenu.add(saveModel);

		loadModel = new JMenuItem("Load Model");
		loadModel.addActionListener(new LoadModelActionListener(model));
		mnistMenu.add(loadModel);

		learnFromModel = new JMenuItem("Learn From Model");
		learnFromModel.addActionListener(new LogRegMenuActionListener(model, view));
		mnistMenu.add(learnFromModel);

		openDNNModel = new JMenuItem("Load model");
		openDNNModel.addActionListener(new OpenDNNModelActionListener(model));
		dnnMenu.add(openDNNModel);

		saveDNNModel = new JMenuItem("Save model");
		saveDNNModel.addActionListener(new SaveDNNModelActionListener(model));
		dnnMenu.add(saveDNNModel);

	}

}
