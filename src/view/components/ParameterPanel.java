package view.components;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import view.WriteNumbers;
import view.WriteNumbersModel;
import view.actionlisteners.GetRandomInputActionListener;
import view.actionlisteners.LearnMenuActionListener;
import view.actionlisteners.ResetActionListener;
import view.actionlisteners.ResetTestActionListener;

public class ParameterPanel extends JPanel {

	JProgressBar progressbar;

	JTextField hiddenNumberText;
	JTextField iterationNumberText;
	JTextField learningRateText;

	public ParameterPanel(WriteNumbersModel model, WriteNumbers view) {
		JLabel hiddenNumberLabel = new JLabel("Number of hidden neurons:");
		this.add(hiddenNumberLabel);
		hiddenNumberText = new JTextField("10");
		hiddenNumberText.setColumns(3);
		this.add(hiddenNumberText);

		JLabel iterationNumberLabel = new JLabel("Number of iterations:");
		this.add(iterationNumberLabel);
		iterationNumberText = new JTextField("250");
		iterationNumberText.setColumns(5);
		this.add(iterationNumberText);

		JLabel learningRateLabel = new JLabel("Learning rate:");
		this.add(learningRateLabel);
		learningRateText = new JTextField("0.01");
		learningRateText.setColumns(5);
		this.add(learningRateText);

		JButton btnLearn = new JButton();
		btnLearn.setText("Learn");
		btnLearn.addActionListener(new LearnMenuActionListener(model, view));
		this.add(btnLearn);

		JButton btnReset = new JButton();
		btnReset.setText("Reset");
		btnReset.addActionListener(new ResetActionListener(view));
		this.add(btnReset);

		JButton btnClearResult = new JButton();
		btnClearResult.setText("Clear");
		btnClearResult.addActionListener(new ResetTestActionListener(view));
		this.add(btnClearResult);

		JButton btnRandomInputFromMnist = new JButton();
		btnRandomInputFromMnist.setText("Random Input");
		btnRandomInputFromMnist.addActionListener(new GetRandomInputActionListener(model));
		this.add(btnRandomInputFromMnist);

		progressbar = new JProgressBar();
		progressbar.setValue(0);
		progressbar.setStringPainted(true);

		this.add(progressbar);

	}

	public String getHiddenNumber() {
		return hiddenNumberText.getText();
	}

	public String getIterationNumber() {
		return iterationNumberText.getText();
	}

	public String getLarningRate() {
		return learningRateText.getText();
	}

	public JProgressBar getProgressBar() {
		return progressbar;
	}

}
