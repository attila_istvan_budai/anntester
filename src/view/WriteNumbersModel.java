package view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.SwingWorker;

import mnistreader.MinstDatasetReader;
import mnistreader.MinstItem;
import utils.MathUtils;

import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.logisticregression.LogisticRegression;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.ann.rbm.DNNOpenCL;
import com.bme.datamining.ann.rbm.DNNOpenCL.DNNBuilder;
import com.bme.datamining.filereader.FloatList;
import com.bme.datamining.filereader.LoadModel;
import com.bme.datamining.filewriter.SaveModel;

import dynamicpanel.DynamicPanel;
import dynamicpanel.DynamicPanelModel;

public class WriteNumbersModel {
	WriteNumbers view;
	private float[] weight;
	// RestrictedBoltzmannMachineOpenCL rbm;
	List<DynamicPanelModel> panelModels;
	DNNOpenCL dnn;
	DNNBuilder dnnBuilder;
	MinstItem[] mnistItems;

	int hiddenNumber;
	int iterationNumber;
	float learningRate;

	List<float[]> logregModels;

	public static final int mnistNumber = 50000;

	public WriteNumbersModel(WriteNumbers view) {
		this.view = view;
		// rbm = new RestrictedBoltzmannMachineOpenCL();
		panelModels = new ArrayList<DynamicPanelModel>();
		dnnBuilder = new DNNBuilder();
	}

	public WriteNumbers getView() {
		return view;
	}

	public int getHiddenNumber() {
		return hiddenNumber;
	}

	public int getIterationNumber() {
		return iterationNumber;
	}

	public float getLearningRate() {
		return learningRate;
	}

	public void writeToFile(File file) {
		Writer writer;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getAbsolutePath()), "utf-8"));
			List<DynamicPanel> panels = view.getTrainingPanelView().getTrainingPanels();
			for (DynamicPanel t : panels) {
				writer.write(t.getModel().toString());
				writer.write("\n");
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void saveModelToFile(File file) {
		Writer writer;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getAbsolutePath()), "utf-8"));
			for (int i = 0; i < getWeight().length; i++) {
				writer.write(getWeight()[i] + " ");
				if (i % hiddenNumber == 0 && i != 0) {
					writer.write("\n");
				}
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void loadModelFromFile(File file) {
		setParameters();
		String line;
		int index = 0;
		FloatList list = new FloatList();

		try {
			BufferedReader br = new BufferedReader(new java.io.FileReader(file.getPath()));
			do {
				line = br.readLine();
				if (line == null || line.equals("")) {

				} else {
					String[] words = line.split(" ");
					for (String w : words) {
						list.add(Float.parseFloat(w));
						index++;
					}
				}

			} while (line != null);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setWeight(list.toArray());
	}

	public void setRandomInput() {
		Random r = new Random();
		float[] data = mnistItems[r.nextInt(mnistItems.length - 1)].data;

		DynamicPanel pane = view.getInputPane();
		pane.getModel().setModel(data, 28, 28);
		pane.updateCells(data, 1);
	}

	public void readFromMnist() {
		panelModels = new ArrayList<DynamicPanelModel>();
		File labelFile = new File("C:\\mnist\\train-labels-idx1-ubyte.gz");
		File imageFile = new File("C:\\mnist\\train-images-idx3-ubyte.gz");
		MinstDatasetReader reader = new MinstDatasetReader(labelFile, imageFile);
		mnistItems = new MinstItem[mnistNumber];
		for (int i = 0; i < mnistNumber; i++) {
			MinstItem item = reader.getTrainingItem((i % 10));
			mnistItems[i] = item;
			float[] data = item.data;

			DynamicPanelModel.DynamicPanelModelBuilder builder = new DynamicPanelModel.DynamicPanelModelBuilder();
			builder.setCellNumber(28, 28);
			builder.setModel(MathUtils.normalizeWithValues(data, 255, 0));
			// builder.setModel(data);
			panelModels.add(builder.createDynamicPanel());
		}
		setVisiblePanels(0, 5);

	}

	// public void setVisiblePanels(int from, int to) {
	// List<DynamicPanel> panels =
	// view.getTrainingPanelView().getTrainingPanels();
	// for (int i = from; i < to; i++) {
	// panels.get(i - from).setModel(panelModels.get(i));
	// }
	// }

	public void setVisiblePanels(int from, int to) {
		for (int i = from; i < to; i++) {
			PanelUpdateWorker worker = new PanelUpdateWorker(i - from, i);
			worker.execute();
		}
	}

	class PanelUpdateWorker extends SwingWorker<Void, Void> {
		private int panelNumber;
		private int modelNumber;

		public PanelUpdateWorker(int panelNumber, int modelNumber) {
			this.panelNumber = panelNumber;
			this.modelNumber = modelNumber;
		}

		@Override
		public Void doInBackground() throws IOException {
			List<DynamicPanel> panels = view.getTrainingPanelView().getTrainingPanels();
			panels.get(panelNumber).setModel(panelModels.get(modelNumber));
			return null;
		}

		@Override
		public void done() {
		}
	}

	public void readFromFile(File file) {
		try {
			BufferedReader br = new BufferedReader(new java.io.FileReader(file.getPath()));
			List<DynamicPanel> panels = view.getTrainingPanelView().getTrainingPanels();
			String line;
			int column = 0, row = 0, matrixNumber = 0;
			FloatList input = new FloatList();

			do {
				line = br.readLine();
				if (line == null || line.equals("")) {
					panels.get(matrixNumber).getModel().setModel(input.toArray(), column, row);
					column = 0;
					row = 0;

					input = new FloatList();

					matrixNumber++;
				} else {
					String[] words = line.split("  ");
					column = words.length;
					for (String w : words) {
						input.add(Float.parseFloat(w));
					}
					row++;
				}

			} while (line != null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void learn() throws IOException {
		setParameters();
		int numberOfElements = panelModels.size();
		int arrayLength = panelModels.get(0).getModel().length;
		float[] trainingSet = new float[numberOfElements * arrayLength];
		for (int i = 0; i < numberOfElements; i++) {
			float[] currentArray = panelModels.get(i).getModel();
			System.arraycopy(currentArray, 0, trainingSet, i * arrayLength, arrayLength);
		}

		if (dnn == null) {
			dnn = dnnBuilder.addFirstLayer(100, arrayLength).addLayer(100).createSingleton();
		}
		// rbm = new RestrictedBoltzmannMachineOpenCL();
		// rbm.registerObserver(new RBMViewObserver(this));
		// setWeight(rbm.train(trainingSet, arrayLength, hiddenNumber,
		// numberOfElements, iterationNumber, learningRate));
		setWeight(dnn.train(trainingSet, arrayLength, numberOfElements, iterationNumber, learningRate));
	}

	public void saveDnnResult() throws IOException {
		SaveModel.saveNewModel(dnn);
	}

	public void loadDnnResult(File folderName) throws IOException {
		dnn = LoadModel.loadModel(folderName);
	}

	public void learnFromModel() throws IOException {
		System.out.println("learnFromModel()");
		logregModels = new ArrayList<float[]>();
		LogisticRegression logreg = new LogisticRegression(new InnerNeuron(new SigmoidFunction()));
		float[] trainingSet = new float[mnistNumber * hiddenNumber];
		float[] trainingLabels = new float[mnistNumber];
		float[] constructedData = new float[mnistNumber * hiddenNumber];

		for (int i = 0; i < mnistItems.length; i++) {
			float[] data = mnistItems[i].data;
			constructedData = dnn.calculateHiddenLayer(data, data.length / (28 * 28));
			// System.arraycopy(constructedData, 0, trainingSet, i *
			// hiddenNumber, hiddenNumber);

		}
		for (int currentNumber = 0; currentNumber < 10; currentNumber++) {
			String current = Integer.toString(currentNumber);
			for (int i = 0; i < mnistItems.length; i++) {
				if (mnistItems[i].label.equals(current)) {
					trainingLabels[i] = 1;
				} else {
					trainingLabels[i] = 0;
				}
			}

			logregModels.add(logreg.learnCL(constructedData, trainingLabels, trainingLabels.length, hiddenNumber, 0.1f, 1000));
			System.out.println("--------------------------------------");
		}
	}

	public void setParameters() {
		hiddenNumber = Integer.parseInt(view.getParameterPanel().getHiddenNumber());
		iterationNumber = Integer.parseInt(view.getParameterPanel().getIterationNumber());
		learningRate = Float.parseFloat(view.getParameterPanel().getLarningRate());
	}

	public void check() {
		DynamicPanel pane = view.getInputPane();
		float[] data = pane.getModel().getModel();

		// RestrictedBoltzmannMachineOpenCL rbm = new
		// RestrictedBoltzmannMachineOpenCL();
		// float[] matrix = rbm.reConstruct(data, getWeight(), data.length,
		// hiddenNumber, 1);
		float[] matrix = dnn.reconstructData(data, 1);

		DynamicPanel resultPane = view.getResultPane();

		// matrix.setColumnLength(resultPane.getModel().getColumnNumber());
		// matrix.setRowLength(resultPane.getModel()).getRowNumber());
		resultPane.updateCells(matrix, 1);

	}

	public void guessNumber() {
		DynamicPanel pane = view.getInputPane();
		float[] data = pane.getModel().getModel();
		LogisticRegression logreg = new LogisticRegression(new InnerNeuron(new SigmoidFunction()));

		// float[] matrix = rbm.stepForward(data, getWeight(), data.length,
		// hiddenNumber, 1);
		float[] matrix = dnn.calculateHiddenLayer(data, 1);

		float maxValue = 0;
		int maxIndex = 0;
		for (int i = 0; i < logregModels.size(); i++) {
			float val = logreg.calculateOutput(matrix, logregModels.get(i));
			System.out.println("index: " + i + " value: " + val);
			if (val > maxValue) {
				maxValue = val;
				maxIndex = i;
			}
		}
		view.lblGuessTheNumber.setText(Integer.toString(maxIndex) + " for " + maxValue);
	}

	public void showModel() {

		DynamicPanel modelResultPanel = view.getModelResultPane();
		float[] arr = MathUtils.normalizeArray(getWeight());
		float[] filtered = MathUtils.filterByPositiveValues(arr, 0.5f);
		// showSeperatedModel(filtered);
		modelResultPanel.updateCells(filtered, hiddenNumber);
	}

	public void showSeperatedModel(float[] weights) {
		float[][] w = MathUtils.seperateArray(weights, hiddenNumber);
		List<DynamicPanel> panels = view.getDetailedResultView().getDetailedResultPanels();
		for (int i = 0; i < hiddenNumber; i++) {
			panels.get(i).updateCells(w[i], 1);
		}
	}

	public float[] getWeight() {
		return weight;
	}

	public void setWeight(float[] weight) {
		this.weight = weight;
	}

	public void addElementToResultGraph(double d, int iteration) {
		if (view.getResultChart() != null) {
			view.getResultChart().addData(d, iteration);
			view.getResultChartZoomed().addData(d, iteration);
		}
	}

}
